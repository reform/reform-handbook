# MNT Reform Operator Handbook

## Licenses

All text, sources, electronics and artwork in this handbook are dual-licensed under:

- GNU GPL v3 or newer
- CERN-OHL-S 2.0

Previously, the sources (incl. text) were licensed under GPL v3+ and the electronics under CERN-OHL-S 2.0, but these licenses are incompatible with each other when combined into a complete work. This is why the book as a whole or its sources can be reproduced under any of these two licenses above.

The artwork (graphics) are additionally licensed under CC BY-SA 4.0.

## Credits

See [credits.rst](src/source/credits.rst).

## Build

### Install Inter font by rsms

```bash
wget https://github.com/rsms/inter/releases/download/v3.15/Inter-3.15.zip
mkdir -p ~/.local/share/fonts
unzip Inter-3.15.zip
mv 'Inter Desktop' ~/.local/share/fonts/
fc-cache ~/.local/share/fonts
```

More info: https://rsms.me/inter/

### Prerequisites: Debian

```bash
sudo apt install python3-sphinx texlive-xetex texlive-latex-extra pandoc inkscape wget unzip fontconfig texlive-font-utils texlive-fonts-recommended ghostscript
```

### Build HTML version

```bash
cd src
make html
firefox build/html/index.html
```

### Build LaTeX/PDF version

```bash
cd src
./build-pdf-book.sh
```

## Style Guide

### Swiss Style

An absolute and universal form of graphic expression through objective and impersonal presentation, communicating to the audience without the interference of the designer's subjective feelings or propagandist techniques of persuasion.

- Grid.
- Flush left align, ragged right.
- Sans-serif
- Objective photography.
