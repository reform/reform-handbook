#!/bin/bash

rm source/_static/illustrations/*.eps

cd source/_inkscape
for f in *.svg
do
	target="${f/refman-/}"
	targeta="../_static/illustrations/${target%.svg}.eps"
	echo "Creating: $targeta"
	inkscape -o "$targeta" "$f"
done

# FIXME manual fixups
cd ../_static/illustrations
