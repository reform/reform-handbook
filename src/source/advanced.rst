Advanced Topics
===============

A computer is a complex system where hardware and software interact, and sometimes, unpredicted problems may appear. Most issues can be fixed, and in this chapter we will show you how.

This is for advanced users only. If you are unsure about what you are doing, we encourage you to get in touch with our MNT Community or our email support (``support@mntre.com``).

Troubleshooting
---------------

There are some steps you can try if MNT Reform should stop booting. As always, before working with MNT Reform internals, first disconnect the internal battery and external power and peripherals.

Boot Issues
+++++++++++

What can you do if your MNT Reform doesn't boot?

There are several steps you can take to determine the cause.

First of all, check if it has power. If you are in luck, a cable or a connector is just loose and you can plug it back in.

If this doesn't work, take a look at the OLED screen. Does it show anything when you press the *Circle* key for at least 2 seconds? If not, this could have several reasons: the OLED module or the two cables of the keyboard may not be fully connected. It is also possible that the keyboard or the mainboard are defective. You can check the functionality of your keyboard by taking it out of the laptop, turning the *STANDALONE* switch on and connecting it to another computer. If it doesn't work on another computer, then the keyboard is most likely the reason why your Reform doesn't boot.

If the keyboard is working, but your MNT Reform still doesn't boot, the next step would be to make sure the SD card is functional and inserted correctly. If that doesn't fix your boot issue and you have another computer at hand, install the latest System Image on your SD card. Refer to the chapter "Software" and the section "Flashing the System Image" in this handbook.

Insert the freshly flashed SD card and try to boot MNT Reform. If this isn't successful, the motherboard or the Processor Module could be defective. If you have another Processor Module and knowledge of how to swap it, you could try this. We recommend getting in touch with our customer support. You have a 2-year warranty on your device. Warranty repairs are free within this period. Please refer to our FAQ (`<https://mntre.com/faq.html>`_) for more details.

Serial Console
++++++++++++++

If you have good Linux knowledge and surmise the boot issue is not hardware related, you can try fixing it using the serial console.

The motherboard connectors labeled *S1* and *S2* are serial ports (UART) to which U-Boot and the Linux kernel output diagnostic information on startup. The correct serial port depends on your Processor Module:

=============== ==== =======
Module          Port Baud
=============== ==== =======
i.MX8MQ         S1   115200
i.MX8MPlus      S1   115200
LS1028A         S1   115200
RCM4-BPi/A311D  S2   115200
RCM4-RPi        S1   115200
RK3588          S1   1500000
=============== ==== =======

Wire up a 3.3V USB-to-UART adapter to the following pins of connector J18 *(S1)* or J20 *(S2)*:

=== ========================================
Pin Function
=== ========================================
1   UART1_TXD, connect to RX of your adapter
2   UART1_RXD, connect to TX of your adapter
3   GND, connect to GND of your adapter
=== ========================================

Then, use a terminal program such as ``tio`` on your host computer:

.. code-block:: none

   tio /dev/ttyUSB0 -b 115200 # Check Module manual
                              # for baud rate

If you then switch on Reform (powered by the wall adapter) with the provided SD card inserted, you should see the U-Boot console in ``tio``.

Electronics Repair
++++++++++++++++++

Before attempting an electronics fix, please contact our customer support at ``support@mntre.com``. Additionally, please consult the MNT Community forum (`<https://community.mnt.re>`_) for similar issues. Check our repair guides if you are knowledgeable with electronics. Open the respective KiCad file, for example the motherboard file, and take a look at the schematics and PCB layout. You can trace the power rails and measure them at different points on the board. You need a digital multimeter for this.

The Reform will accept DC power between 9 and 32 volts into its barrel jack that is marked J1 on the motherboard. The barrel plug that the Reform accepts has an outer diameter of 5.5 mm and an inner diameter of 2.1 mm with a positive inner contact (IEC 60130-10:1971 Type A). The nominal input voltage is 24V. If you can't measure the input voltage on R49, check if fuse F1 is blown.

Either wall or battery power will be regulated to ~29V by the buck-boost regulator/charger LTC4020 and output to the main system regulators. U14 is the always-on 3.3V regulator that powers critical chips like the System Controller (LPC11U24, U18). You can confirm *LPC_VCC* power with 3.3V on J22 pin 15.

Two white indicator LEDs on the motherboard, D11 and D12, signal that 3.3V and 5V rails are turned on, respectively. Because of the level shifters U28 and U8, booting from SD card requires both 3.3V and 1.8V rails to work. You can measure 1.8V on C130, for example.

The USB hub U9 and the MIPI to eDP converter U10 also need 1.2V power to work (measure on C37). The display itself requires the *3V3_PWR_AUX* (3.3V) and *USB_PWR* (5V) rails to be switched on by the System Controller.

System Controller
+++++++++++++++++

The System Controller has to have working firmware to turn on the main power rails. If it is not responding to commands from the OLED menu, the first thing you can do is pressing the *LPC_RST* button on the motherboard to reset the System Controller.

For debugging, you can use a UART adapter on *SYSCTL* header J23 to talk to the System Controller directly (57600 baud 8N1). To do this, you'll need to solder/crimp an adapter cable with a JST-PH plug.

Software Issues
+++++++++++++++

If you suspect software issues, run the ``reform-check`` command (see "Software" chapter, "Reform Tools" section in this handbook).

System Boot
-----------

Unlike a PC that uses BIOS, Reform utilizes U-Boot, a bootloader commonly used on ARM processors. At the time of writing, Reform only supports ARM based modules.

The Processor Module will usually try to load boot code from the SD card, possibly depending on the setting of a DIP switch on the your specific module. MNT Reform comes with U-Boot on the SD Card. Some MNT Reform Processor Modules can boot directly from other media such as eMMC flash. Refer to the module documentation for further information.

U-Boot is like a mini operating system and shell that allows you to inspect parts of the system (like PCIe, USB devices or Ethernet) and set up parameters to be passed to the real operating system kernel such as Linux, and start it. Every module has its own U-Boot version, specifically adapted for Reform. The sources and build instructions for all versions can be found at: `<https://source.mnt.re/reform>`_

U-Boot needs 3 files to boot Linux:

- The Linux kernel itself, named ``vmlinuz-...-reform2-arm64``.
- The device tree blob (DTB). The file has a Processor Module specific file name ending with ``.dtb``. The device tree is a data structure that lists the addresses of and parameters for all the devices in the system that Linux needs to initialize drivers for.
- U-Boot looks for a file called ``boot.scr`` on the SD card, which is a script of commands that performs the actual loading of the OS. This script is normally managed by Debian and has a binary header. Usually, you never need to edit it yourself. If you really need to and know what you're doing, a tool called ``mkimage`` is required to update it.

Theoretically, you can boot other operating systems and Linux distributions besides Debian, such as FreeBSD, NetBSD, OpenBSD---if they include all necessary drivers for your Processor Module and provide an ``extlinux.conf`` file for U-Boot. Refer to `<https://docs.u-boot.org>`_ for details. Booting another Linux distribution can be achieved by combining the MNT Reform Linux kernel and DTB on the boot partition with the distribution's root file system extracted to the second partition.
