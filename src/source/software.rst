Software
========

In the first edition of the handbook, we listed a number of free open source applications that had been tested and pre-installed on MNT Reform. While this was intended to add to the user experience, it also contributed to the system becoming a bit cluttered. Nowadays, we ship the system in a more minimalist form and give you the freedom to decide what to install. We still wanted the computer to be practical out of the box, so we included some essential tools that are listed in the following section. We also included a section about the so-called "Reform Tools", which are Reform laptop specific helpers and scripts.

MNT Reform can run most Linux applications that are available for 64bit ARM-based computers (also called AArch64 or ARM64).

Preinstalled Software
---------------------

Evince
  View PDFs and other documents.

  Command: ``evince``

  `<https://wiki.gnome.org/Apps/Evince>`_

Firefox
  Mozilla's open-source web browser.

  Command: ``firefox``

  `<https://www.mozilla.org/firefox>`_

foot
  A terminal emulator that is fast, minimalistic and lightweight.

  Command: ``foot``

  Keyboard shortcut: *SUPER+ENTER*

  `<https://codeberg.org/dnkl/foot>`_

GNOME Disks
  Partition, format, and manage internal and external disks.

  Command: ``gnome-disks``

  `<https://wiki.gnome.org/Apps/Disks>`_

Grim & Slurp
  Grim is a wayland-based screenshot tool, and Slurp lets you select the region of the screen you want to capture.

  Command: ``grim -g "$(slurp)"``

  Keyboard shortcut: *SUPER+SHIFT+X*

  `<https://wayland.emersion.fr/grim>`_

htop
  A terminal-based look at system processes, CPU and memory usage.

  Command: ``htop``

  `<https://htop.dev>`_

micro
  An accessible, fresh terminal-based text editor.

  Command: ``micro``

  `<https://micro-editor.github.io/>`_

Minetest
  An 3D block-based open-world game focused on exploration, resource gathering, and construction.

  Command: ``minetest``

  `<https://minetest.net>`_

MPV
  A versatile, terminal-based media player.

  Command: ``mpv a-movie-file.mp4``

  `<https://mpv.io>`_

NCurses Disk Usage
  Terminal-based disk usage analysis utility.

  Command: ``ncdu``

  `<https://dev.yorhel.nl/ncdu>`_

Neverball
  This 3D game lets you guide a ball through 24 challenging levels.

  Command: ``neverball``

  `<https://neverball.org>`_

Sxiv
  A simple and fast image viewer.

  Command: ``sxiv``

  `<https://github.com/muennich/sxiv/>`_

Thunar
  Lightweight file manager.

  Command: ``thunar``

  Keyboard shortcut: *SUPER+T*

  `<https://docs.xfce.org/xfce/thunar/start>`_

Vim
  The other major open text editor.

  Command: ``vim`` (Exit by typing ``:q`` followed by *ENTER*)

  `<https://www.vim.org>`_

Wayland Event Viewer
  Useful when debugging input devices under wayland-based compositors such as Sway.

  Command: ``wev``

  `<https://git.sr.ht/~sircmpwn/wev>`_

Wayvnc
  Access your MNT Reform desktop remotely from VNC clients on other platforms using this server application.

  Command: ``wayvnc``

  `<https://github.com/any1/wayvnc>`_

wf-recorder
  Record MP4 video of your desktop with this program.

  Command: ``wf-recorder`` (Stop with *CTRL+C*. The resulting video is named "recording.mp4")

  `<https://github.com/ammen99/wf-recorder>`_

Updating Software
---------------------

We recommend updating your software on a regular basis as bugs get fixed, new features are introduced and improvements are made. For this purpose, Debian has an integrated package manager called ``apt``. You can stay up to date by executing the following command: ``sudo apt update``. This will show you how many packages can be updated. You can either update individual packages or everything at once with one command: ``sudo apt upgrade``.

Flashing the System Image
-------------------------

If your SD card is broken or you want to set up a new system (for example when upgrading to a new Processor Module), you can download the "System Image" from: `<https://mnt.re/system-image>`_

**First of all, back up all your important data!** The following process will erase everything that is on the SD card you decide to flash.

From the link above, download the file whose name matches the Processor Module in your MNT Reform:

======================= ================================
Processor Module        Filename
======================= ================================
NXP i.MX8MQ             ``reform-system-imx8mq.img.gz``
NXP i.MX8MPlus          ``reform-system-imx8mp.img.gz``
NXP LS1028A             ``reform-system-ls1028a.img.gz``
RCM4 with BPi CM4/A311D ``reform-system-a311d.img.gz``
Rockchip RK3588         ``reform-system-rk3588.img.gz``
======================= ================================

Unzip the image using gunzip (substitute your actual image file name, also in the following steps):

.. code-block:: none

 gunzip reform-system-imx8mq.img.gz

Identify the device name of your SD card. For example, you can check the output of ``lsblk`` before and after you insert the card and compare which new device appears. Below we use the device sdX, but it will be a different one on your computer.

Copy the image to your SD card **(Warning: all data on the SD card will be erased!)**:

.. code-block:: none

 sudo dd if=reform-system-imx8mq.img of=/dev/sdX \
         bs=8M status=progress

That's it---now you can boot your Reform from the freshly flashed SD card.

As new modules become available, you can refer to our online guide: `<https://source.mnt.re/reform/reform-system-image#downloading-and-flashing-a-system-image>`_

Reform Tools
------------

We have our own Debian package called ``reform-tools``. These tools are designed to automate otherwise tedious system configuration tasks, so we encourage you to check them out.

.. table::
    :widths: 30 40

    +-------------------------+--------------------------------------------------------------------------------+
    |Command                  |Function                                                                        |
    +=========================+================================================================================+
    |``reform-check``         |Tries to analyze what's wrong with your OS.                                     |
    +-------------------------+--------------------------------------------------------------------------------+
    |``reform-display-config``|This tool configures single and dual display mode. Only supported with i.MX8MQ. |
    +-------------------------+--------------------------------------------------------------------------------+
    |``reform-flash-uboot``   |This updates the bootloader (rarely needed).                                    |
    +-------------------------+--------------------------------------------------------------------------------+
    |``reform-help``          |Shows you a list of useful commands.                                            |
    +-------------------------+--------------------------------------------------------------------------------+
    |``reform-migrate``       |Helps you with OS migration from SD card to NVME or eMMC flash.                 |
    +-------------------------+--------------------------------------------------------------------------------+

More information about reform-tools:
`<https://source.mnt.re/reform/reform-tools>`_

Binary (In)compatibility
------------------------

A popular architecture for computers is x86-64 (a.k.a. AMD64). Binaries compiled for this architecture are incompatible with ARM processors. If you want to use binary software, you have to make sure that it is built for AArch64. The vast majority of open-source software is available for AArch64, but there can be subtle problems when x86 is implicitly expected, for example:

- Optimizations written in assembler (machine code), targeting specific SIMD/vector instructions
- JIT (just-in-time) compilers

Generally, instead of using inline assembler or targeting a single architecture directly, use cross-platform libraries and code-emitting backends.

Running x86 Software
--------------------

Until RISC architecture will change everything, many games and proprietary closed-source applications are only available as x86 or x86-64 binaries. If you need to run such a binary, you can try binary translators such as ``box64`` (available in ``apt``) or ``DEX``:

.. code-block:: none

 box64 ./my-game

You can even run some Microsoft Windows applications on MNT Reform by chaining ``box64`` and ``wine``, which is a Linux compatibility layer for Windows binaries:

.. code-block:: none

 box64 wine64 ./my-game

Emulating 32-bit x86 binaries is a bit more complicated, but possible on a 64-bit ARM platform. You can leverage Debian's "multiarch" feature to add the ``armhf`` architecture to your system and install ``box86`` (``box64``'s 32-bit sibling) this way. For troubleshooting such setups, don't hesitate to get in touch with the MNT Community.

GPU Hacks
---------

At the time of writing, most MNT Reform Processor Modules are based on ARM System-on-Chips that contain a GPU meant for mobile or "embedded" use. Multiple factors define the limits of the versions of OpenGL, OpenGL ES and Vulkan APIs that software can use on MNT Reform: for example, the space- or energy-saving architecture of these GPUs, or the capabilities of the often reverse-engineered open-source drivers such as ``etnaviv`` or ``panfrost``.

Sometimes, graphics heavy applications or 3D games are written with large and power hungry desktop GPUs in mind and require a GL or Vulkan version that is out of reach of your device. In some cases the developers do not really require the features available for higher OpenGL versions, though, and you can try to work around the limitation by pretending that your system supports a higher version than it really does. To do this, you can set the following environment variables in a terminal and then launch your application or game from this same terminal:

.. code-block:: none

 export MESA_GL_VERSION_OVERRIDE=4.2
 export MESA_GLSL_VERSION_OVERRIDE=420
 my-game

If this fails, you can always fall back to software emulation of OpenGL, which will be slow but sometimes usable:

.. code-block:: none

 export LIBGL_ALWAYS_SOFTWARE=1
 my-game

Mesa is the collection of Linux 3D graphics drivers. You can learn more about various Mesa environment variables at `<https://docs.mesa3d.org/envvars>`_
