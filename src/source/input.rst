Input Devices
=============

MNT Reform comes with a keyboard and either a trackball or a trackpad preinstalled. All of the input devices are modular and can be swapped in and out. They all connect via internal USB 2.0 cables.

Keyboard
--------
.. image:: _static/illustrations/4-keyboard-v3-special-keys.png

With our newest keyboard [#]_, we designed in all necessary concave keycap sizes to achieve a traditional stagger, and a contiguous convex space bar which is easier on the thumbs. It is still split, but you can hit it without even thinking about it. If you're an advanced user, you can also remap two of these space keys. Additionally, we included two homing keycaps to improve navigation while touch typing.

Because many advanced users remap the traditional Caps Lock key to a different function, we swapped Caps Lock for a *CTRL* key. This makes the use of *CTRL* key combinations more ergonomic.

Next to the new *AGR* key---short for "Alternate Graphic", located next to the space bar---is a key with 3 dots (an ellipsis). It is intended as a user-defined key, which is mapped to *Right CTRL* by default. We recommend remapping this key to *Compose*, a key that allows you to generate Unicode symbols from Compose sequences.

Lastly, MNT Reform features an additional modifier key, the *HYPER* key, in the lower left. *HYPER* provides an additional layer of key combinations. Here is the list of shortcuts you can use with *HYPER*:

=========== ===================
Shortcut    Function
=========== ===================
*HYPER+F1*  Decrease Brightness
*HYPER+F2*  Increase Brightness
*HYPER+F7*  Previous Track
*HYPER+F8*  Play
*HYPER+F9*  Next Track
*HYPER+F10* Silent
*HYPER+F11* Volume Down
*HYPER+F12* Volume Up
*HYPER+←*   Home
*HYPER+→*   End
*HYPER+↑*   Page Up
*HYPER+↓*   Page Down
=========== ===================

.. [#] If you happen to own one of the first MNT Reform laptops, you might have noticed a slightly unusual layout of the keyboard. Back in the day, we simplified the traditional typewriter-based layout so that the keyboard could be constructed using only two distinct key shapes that were available to us (square and 1.5x). The biggest difference was the split space bar: instead of one long key, there were two 1.5x wide space keys, with left and right Alt keys sandwiched between them. However, this proved to be too experimental a layout for some people to get used to.

OLED Menu
---------

.. image:: _static/illustrations/4-oled-menu.png

The keyboard has a built-in OLED display for interaction with the System Controller on the motherboard. You can highlight an option and scroll through the menu by using the *↑* and *↓* keys. To trigger the highlighted option, press *ENTER*. Alternatively, you can press the shortcut key that is displayed on the right hand side of each menu option. For example, to show the Battery Status, press *B* when the menu is active. To leave the menu, press *ESC*.

.. image:: _static/illustrations/4-oled-battery-status.png

You can see detailed battery information including the estimated total charge percentage on the Battery Status screen reachable through the OLED menu. Each cell icon corresponds to one of the eight battery cells. The leftmost group of four icons represents the battery pack on the left side of the device, and the group on the right represents the battery pack on the right---assuming you look at MNT Reform when flipped on its back and the batteries closest to you.

For the left pack, the top battery icon represents the leftmost cell in the pack, and the bottom icon represents the rightmost cell. For the right pack, it is the other way around, because the pack is rotated by 180 degrees in the laptop.

.. image:: _static/illustrations/4-oled-battery-mapping.png

The illustration shows which icon and voltage on the OLED display corresponds to which battery cell in the device.[#]_

.. [#] Note that this order is only valid for the Protected Battery Boards.

Trackball
---------
.. image:: _static/illustrations/4-trackball-buttons.png

The trackball works like a mouse with three buttons (left, middle, and right click). Roll the ball to move the cursor. In addition to the standard mouse buttons, the trackball also has two *Scroll Mode* buttons. Holding down either while moving the ball will scroll the currently focused content. Pressing both *Scroll Mode* buttons together will turn on *Sticky Scroll Mode*, so that you can scroll through a longer text without needing to hold down a button.

Trackpad
--------

.. image:: _static/illustrations/4-trackpad-touch-surface.png

Unlike the trackball, the trackpad doesn't have any buttons. Slide one finger across the surface to move the cursor. Tapping with one finger acts like a left mouse button click. Slide two fingers across the surface to scroll in any direction. Tapping with two fingers performs a right click. To click and drag, use three fingers. Tapping with three fingers is equivalent to a middle click.
