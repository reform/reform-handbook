.. role:: raw-latex(raw)
   :format: latex

Preface to the Second Edition
+++++++++++++++++++++++++++++

A second edition of the MNT Reform Operator Handbook---so soon? Well, we are astonished, too! The past four years went by so quickly and a lot has changed since we first introduced our MNT Reform laptop, so it's about time to provide a fresh, up-to-date handbook.

At the time of writing, more than 500 people call the MNT Reform their own---amazing! Your support means a lot to us and keeps us going.

Since the initial release of Reform, we have developed several Processor Modules, updated software, firmware and hardware, and made changes based on open source contributions from the MNT community. We have created quite a few Reform products by now, such as the Reform Standalone Keyboard, the Reform Camera and the Pocket Reform. They are all compatible with each other, forming the MNT Reform family.

With MNT Reform, we provide open and sustainable alternatives to today's closed hardware ecosystems. Our devices are meant to be understood and improved by anyone---and community is a vital element in this context. By helping each other and working together we have a chance to create a future where accessible open hardware thrives. Many of you participate in the MNT Community [#]_, actively sharing the progress of your customizations, additions of new features or modifications to your Reform(s). Feel free to drop by---we'd like to see you there!

.. [#] `<https://community.mnt.re>`_

Preface to the First Edition
++++++++++++++++++++++++++++

Early in the MNT Reform project, we made---working in the kitchen---13 prototypes for us and a handful of adventurous people. These were much more primitive predecessors of the laptop that you received today, based on an older processor and an eclectic mix of materials. Each already came with an *Operator Handbook:* 30 manually bound pages of schematics and instructions for handling the prototype. It was but a rough sketch of the book that you are reading now.

The original handbook turned out to be an icebreaker and conversation aid when talking about MNT Reform with other people. We wrote this new handbook in the same spirit: to make your MNT Reform more approachable, understandable, but also more discussable. The book will be your companion when taking MNT Reform apart and learning the basics of using the machine.

At a time when electronics are becoming ever more secretive, MNT Reform and this book buck the trends---but they are not sacred artifacts. They are meant to be taken apart, discussed, hacked, built upon. Scribble in the margins. Make it your own.

:raw-latex:`\mainmatter`
